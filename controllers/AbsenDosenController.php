<?php

namespace app\controllers;

use Yii;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\AbsenDosen;
use app\models\KalenderAkademik;
use app\models\Kelas;
use app\models\Prodi;

/**
 * AbsenController implements the CRUD actions for AbsenDosen model.
 */
class AbsenDosenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AbsenDosen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $day  = ['senin', 'selasa', 'rabu', 'kamis', 'jum\'at'];
        $days = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at']; 

        if (\Yii::$app->request->isPost) {
            $ka     = $_POST['Kelas']['ka_id'];
            $prodi  = $_POST['Kelas']['prodi_id'];
            $mk     = $_POST['Kelas']['mk_id'];
        
            for ($i = 0; $i < count($day); $i++) {
                $query = Kelas::find()->where(['day' => $day[$i]]);
                $query->andFilterWhere([
                    'ka_id' => $ka,
                    'prodi_id' => $prodi,
                    'mk_id' => $mk,
                ]);
                $dataProvider[$i] = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => false,
                ]);
            } 

            $ka = KalenderAkademik::findOne($ka)->name;
            $prodi = Prodi::findOne($prodi)->name;

            $model = new Kelas();

            return $this->render('index', [
                'dataProviders' => $dataProvider,
                'model' => $model,
                'day' => $days,
                'ka' => $ka,
                'prodi' => $prodi
            ]);
        }

        else {
            for ($i = 0; $i < count($day); $i++) {
                $dataProvider[$i] = new ActiveDataProvider([
                    'query' => Kelas::find()->where(['day' => $day[$i]]),
                    'pagination' => false,
                ]);
            }
        }

        $model = new Kelas();

        return $this->render('index', [
            'dataProviders' => $dataProvider,
            'model' => $model,
            'day' => $days,
            'ka' => 'UN',
            'prodi' => 'FILTERED'
        ]);
    }
    
    public function actionExport() 
    {
        $sql = "SELECT d.name AS dosen, mk.name AS mata_kuliah, COUNT(ad.conference_for) AS cf 
                FROM dynamic_absen_dosen ad 
                LEFT OUTER JOIN static_dosen d ON ad.dosen_id = d.id 
                LEFT OUTER JOIN static_kelas k ON ad.kelas_id = k.id
                LEFT OUTER JOIN static_matakuliah mk ON k.mk_id = mk.id
                GROUP BY ad.dosen_id, ad.kelas_id ORDER BY dosen ASC ";

        if (($database = Yii::$app->db->createCommand($sql)->queryAll()) != null) {

            $spreadsheet = new PhpSpreadsheet\Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);
            $worksheet = $spreadsheet->getActiveSheet();

            $styleBody = array(
                'borders' => array(
                    'outline' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => array('argb' => '0000000'),
                    ),
                ),
            );

            $styleHeader = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => array(
                    'outline' => array(
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => array('argb' => '0000000'),
                    ),
                ),
            ];

            $worksheet->setCellValue('A1', 'Nama Dosen');
            $worksheet->setCellValue('B1', 'Mata Kuliah');
            $worksheet->setCellValue('C1', 'presensi');

            $worksheet->getStyle('A1')->applyFromArray($styleHeader);
            $worksheet->getStyle('B1')->applyFromArray($styleHeader);
            $worksheet->getStyle('C1')->applyFromArray($styleHeader);

            $x= 2;

            for($i = 0; $i < count($database); $i++) {
                $worksheet->setCellValue('A'.$x, $database[$i]['dosen']);
                $worksheet->setCellValue('B'.$x, $database[$i]['mata_kuliah']);
                $worksheet->setCellValue('C'.$x, $database[$i]['cf'].'/14');

                $worksheet->getStyle('A'.$x)->applyFromArray($styleBody);
                $worksheet->getStyle('B'.$x)->applyFromArray($styleBody);
                $worksheet->getStyle('C'.$x)->applyFromArray($styleBody);
                $x++;
            }

            foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {

                $spreadsheet->setActiveSheetIndex($spreadsheet->getIndex($worksheet));
            
                $sheet = $spreadsheet->getActiveSheet();
                $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                /** @var PHPExcel_Cell $cell */
                foreach ($cellIterator as $cell) {
                    $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
                }
            }

            $writer = new Xlsx($spreadsheet);
            $writer->save('excel/rekap.xls');

            return $this->redirect('download');
        }

        throw new NotFoundHttpException('The requested data does not exist in our record.');
    }

    public function actionDownload()
    {
        Yii::$app->response->sendFile('excel/rekap.xls');
    }

    public function actionThrow()
    {
        throw new NotFoundHttpException("Your request is not supported by our team.");
    }

    /**
     * Displays a single AbsenDosen model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AbsenDosen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($kelas = null, $dosen = null)
    {
        $model = new AbsenDosen();
        $kelasModel = Kelas::findOne($kelas);

        $model->kelas_id = $kelas;
        $model->dosen_id = $dosen;
        $model->hour_start = $kelasModel->start_at;
        $model->hour_end = $kelasModel->end_at;
        $model->occurred_at = date('Y-m-d');

        $sql = "SELECT COUNT(id) AS jml FROM dynamic_absen_dosen ad WHERE dosen_id = '$dosen' AND kelas_id = $kelas";

        if (($database = Yii::$app->db->createCommand($sql)->queryAll()) != null) {
            $cf = $database[0]['jml'];
            $model->conference_for = $cf + 1;
        } else {
            $model->conference_for = 0 + 1;
        }
        if ($model->load(\Yii::$app->request->post())) {
            if ($model->save(false)) {
                return $this->redirect(\Yii::$app->request->referrer ?: \Yii::$app->homeUrl);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the AbsenDosen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AbsenDosen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AbsenDosen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
