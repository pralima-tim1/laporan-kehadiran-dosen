<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AbsenDosen */

$this->title = 'Update Absen Dosen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Absen Dosens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="absen-dosen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
