<?php

use app\models\AbsenDosen;
use app\models\Dosen;
use app\models\KalenderAkademik;
use app\models\Matakuliah;
use app\models\Prodi;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AbsenDosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Presensi Dosen & Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Akademik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="absen-dosen-index">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><div class="pull-left"><i class="fas fa-clock" style="padding-right: 1rem;"></i></div> Presensi Dosen & Mahasiswa</h3>
        </div>
        <div class="panel-body row">
            <div class="row" style="margin: 2rem 0;">
                <div class="col-md-12">
                    <?php $form = ActiveForm::begin(['layout' => 'inline']) ?>
                        <?= $form->field($model, 'ka_id')->widget(Select2::className(),[
                            'data' => ArrayHelper::map(KalenderAkademik::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => 'Pilih Kalender Akademik...',
                                'id' => 'ka-id',
                            ]
                        ])->label(false) ?>
                        <?= $form->field($model, 'prodi_id')->widget(Select2::className(),[
                            'data' => ArrayHelper::map(Prodi::find()->all(), 'id', 'abbr'),
                            'options' => [
                                'placeholder' => 'Prodi',
                                'id' => 'prodi-id'
                            ],
                        ])->label(false) ?>
                        <?= $form->field($model, 'mk_id')->widget(Select2::className(),[
                            'data' => ArrayHelper::map(Matakuliah::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => 'Mata kuliah',
                                'id' => 'mk-id'
                            ],
                        ])->label(false) ?>
                        <div class="form-group">
                            <?= Html::submitButton('Tampilkan Jadwal', ['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::a('Reset Jadwal', '/absen-dosen/index', ['class'=>'btn btn-warning']) ?>
                        </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
            <div class="row" style="margin: 2rem 0;">
                <div class="col-md-5">
                    <span class="well well-sm label-default" style="padding: .5rem;">
                        <?= $ka . ' - ' . $prodi?>
                    </span>
                </div>
                <div class="col-md-5 pull-right">
                    <div class="pull-right btn-toolbar">
                        <?= ButtonDropdown::widget([
                            'label' => 'Cetak Rekap',
                            'dropdown' => [
                                'items' => [
                                    ['label' => '<i class="fa fa-file-excel" style="color: green;"></i> Excel', 'url' => '/absen-dosen/export'],
                                    ['label' => '<i class="fa fa-file-pdf" style="color: red;"></i> PDF', 'url' => '/absen-dosen/throw'],
                                ],
                                'encodeLabels' => false,
                            ],
                            'options' => [
                                'class' => 'btn btn-sm btn-default'
                            ]
                        ]) ?>
                        <?= Html::a('Cetak Detail Presensi ', '/absen-dosen/throw', ['class'=>'btn btn-sm btn-default']) ?>
                        <?= Html::a('Cetak Presensi Mahasiswa', '/absen-dosen/throw', ['class'=>'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
            <div style="margin: 2rem 0;">
                <?php $i = 0; ?>
                <?php foreach ($dataProviders as $dataProvider):?>
                    <div class="_tbl">
                    <h3 style="margin-left: .5em;"><?= $day[$i] ?></h3>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'Jam',
                                'value' => function($dataProvider) {
                                    $jam = implode(' - ', [date('H:i', strtotime($dataProvider->start_at)), date('H:i', strtotime($dataProvider->end_at))]);
                                
                                    return $jam;
                                },
                            ],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'Mata Kuliah',
                                'headerOptions' => [
                                    'style' => 'text-align: left;'
                                ],
                                'value' => function($dataProvider) {
                                    $mk = Matakuliah::findOne($dataProvider->mk_id)->name;
                                    $sm = date('Y') - $dataProvider->semester;
                                    $sm = str_split($sm);
                                    $sm = $sm[2] . $sm[3];

                                    if ($dataProvider->prodi_id == 1) {
                                        $prodi = 'TI';
                                    }
                                    if ($dataProvider->prodi_id == 2) {
                                        $prodi = 'SI';
                                    }
                                    $kelas = sprintf('%s%s01', $prodi, $sm);
                                
                                    return "<div style='text-align: left;'>$mk<br><span class='label' style='background-color:#f2dede; color:#c7254e;'>$kelas</span></div>";
                                },
                                'format' => 'html'
                            ],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'Semester',
                                'value' => function($dataProvider) {
                                
                                    return $dataProvider->semester;
                                },
                            ],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'SKS',
                                'value' => function($dataProvider) {
                                    $sks = Matakuliah::findOne($dataProvider->mk_id)->sks;
                                    $semester = 3; //Kelas::findOne($ad->kelas_id)->;
                                
                                    return $sks;
                                },
                            ],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'Dosen',
                                'headerOptions' => [
                                    'style' => 'text-align: left;'
                                ],
                                'contentOptions' => [
                                    'style' => 'text-align: left;'
                                ],
                                'value' => function($dataProvider) {
                                    $dosen = strtolower(Dosen::findOne($dataProvider->dosen_id)->name);
                                
                                    return "<p class='text-capitalize'>$dosen</p>";
                                },
                                'format' => 'raw',
                            ],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'Daftar Presensi',
                                'value' => function() {
                                    return '<div class="">'.Html::button('DOSEN', ['class'=>'btn btn-sm btn-default disabled']) .' '. Html::button('KELAS', ['class'=>'btn btn-sm btn-default disabled']) . '</div>';
                                },
                                'format' => 'raw'
                            ],
                            [
                                'class' => 'yii\grid\DataColumn',
                                'label' => 'Presensi',
                                'value' => function($dataProvider) {
                                    $dosen = $dataProvider->dosen_id;
                                    $kelas = $dataProvider->id;
                                    $sql = "SELECT COUNT(id) AS jml FROM dynamic_absen_dosen ad WHERE dosen_id = '$dosen' AND kelas_id = $kelas";
                                    if (($database = Yii::$app->db->createCommand($sql)->queryAll()) != null) {
                                        $tmke = $database[0]['jml'];
                                        if ($tmke == 14) {
                                            return "$tmke/14 - ".Html::a(
                                                'terpenuhi', 
                                                [
                                                    '/absen-dosen/create', 
                                                    'kelas' => $dataProvider->id, 
                                                    'dosen' => $dataProvider->dosen_id
                                                ], 
                                                [
                                                    'class' => 'btn btn-sm btn-success _modal-btn disabled',
                                                    'style' => 'margin-left: .2rem;'
                                                ]
                                            );
                                        }
                                        return "$tmke/14 - ".Html::a(
                                            '<i class="fas fa-plus"></i> Presensi', 
                                            [
                                                '/absen-dosen/create', 
                                                'kelas' => $dataProvider->id, 
                                                'dosen' => $dataProvider->dosen_id
                                            ], 
                                            [
                                                'class' => 'btn btn-sm btn-success _modal-btn',
                                                'style' => 'margin-left: .2rem;'
                                            ]
                                        );
                                    }
                                    return "0/14 - ".Html::a(
                                        '<i class="fas fa-plus"></i> Presensi', 
                                        [
                                            '/absen-dosen/create', 
                                            'kelas' => $dataProvider->id, 
                                            'dosen' => $dataProvider->dosen_id
                                        ], 
                                        [
                                            'class'=>'btn btn-sm btn-success _modal-btn',
                                            'style' => 'margin-left: .2rem;'
                                        ]
                                    );
                                },
                                'format' => 'raw',
                            ],
                        ],
                    ]); ?>
                    </div>
                <?php $i = $i + 1; ?>    
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>

<?php



Modal::begin([
    'id' => 'modal',
    'header' => 'Tambah Form Presensi',
    'class' => 'fade in'
]);
echo "<div id='content' class=''></div>";
Modal::end();

$this->registerJsFile(
    '@web/js/table-hide.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJs("
    $(function(){
        $('._modal-btn').click(function (){
            $.get($(this).attr('href'), function(data) {
              $('#modal').modal('show').find('#content').html(data)
           });
           return false;
        });
    });
");
