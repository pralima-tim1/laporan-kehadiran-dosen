<?php

use app\models\Dosen;
use app\models\KalenderAkademik;
use app\models\Kelas;
use app\models\Matakuliah;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AbsenDosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="absen-dosen-form">

    <?php $form = ActiveForm::begin([
    ]); ?>
        <div class="row">
            <div class="col-md-6">
                <label>Mata Kuliah</label>
                <p class="form-group"><?= Matakuliah::findOne(Kelas::findOne($model->kelas_id)->mk_id)->name ?></p>
            </div>
            <div class="col-md-6">
                <label>Semester</label>
                <p class="form-group"><?= KalenderAkademik::findOne(Kelas::findOne($model->kelas_id)->ka_id)->name ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>Tanggal</label>
                <?= $form->field($model, 'occurred_at', [
                    'template' => '{label}<span class="input-group-addon"><i class="fas fa-calendar"></i></span>{input}{error}{hint}',
                    'options' => [
                        'class' => 'input-group'
                    ]
                ])->textInput(['disabled' => true, 'readonly' => true])->label(false) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'conference_for')->textInput(['disabled' => true, 'readonly' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label>Jam Mulai</label>
                <?= $form->field($model, 'hour_start', [
                    'template' => '{label}{input}<span class="input-group-btn"><button class="btn btn-info btn-flat"><i class="fas fa-clock"></i></button></span>{error}{hint}',
                    'options' => [
                        'class' => 'input-group'
                    ]
                ])->textInput(['disabled' => true])->label(false) ?>
            </div>
            <div class="col-md-6">
                <div>
                    <label>Jam Akhir</label>
                    <?= $form->field($model, 'hour_end', [
                        'template' => '<div class="input-group">{label}{input}<span class="input-group-btn"><button class="btn btn-info btn-flat"><i class="fas fa-clock"></i></button></span></div>{error}{hint}',
                        'options' => [
                            'class' => 'input-group'
                        ]
                    ])->textInput(['disabled' => true])->label(false) ?>
                </div>
            </div>
        </div>
        <div class="form-group disabled">
            <label>Dosen</label>
            <p class="form-control"><?= Dosen::findOne($model->dosen_id)->name ?></p>
        </div>
        <?= $form->field($model, 'theory')->textarea(['rows' => 6, 'placeholder' => 'Masukan Materi...']) ?>

        <?= $form->field($model, 'dosen_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'kelas_id')->hiddenInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Batal', '/absen-dosen/index', ['class' => 'btn btn-warning']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
