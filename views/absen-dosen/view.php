<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AbsenDosen */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Absen Dosens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="absen-dosen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tgl',
            'jam_masuk',
            'jam_pulang',
            'materi:ntext',
            'tmke',
            'kelas_id',
            'dosen_id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'absen_in',
            'absen_out',
            'bulan_bayar',
            'tahun_bayar',
        ],
    ]) ?>

</div>
