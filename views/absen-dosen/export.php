<?php

use app\models\AbsenDosen;
use app\models\Dosen;
use app\models\Kelas;
use kartik\grid\GridView;

?>
<div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'panel' => false,
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\DataColumn',
            'label' => 'Mata Kuliah',
            'value' => function($dataProvider) {
                $ad = AbsenDosen::findOne($dataProvider->id);
                $matkul = $ad->materi;
                $kelas = Kelas::findOne($ad->kelas_id)->name;

                return $matkul;
            },
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'label' => 'Dosen',
            'value' => function($dataProvider) {
                $ad = AbsenDosen::findOne($dataProvider->id);
                $dosen = Dosen::findOne($ad->kelas_id)->name;

                return $dosen;
            },
        ],
        [
            'class' => 'kartik\grid\DataColumn',
            'label' => 'Presensi',
            'value' => function($dataProvider) {
                $ad = AbsenDosen::findOne($dataProvider->id);
                $tmke = $ad->tmke;
                return "$tmke / 14";
            },
        ],
    ],
]); ?>
</div>