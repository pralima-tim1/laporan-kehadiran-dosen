<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="content container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Made with <i class="fas fa-heart" style="color: red;"></i> &amp; <i class="fas fa-coffee" style="color: #3b2f2f;"></i> by <a href="nurulfikri.ac.id">BTSI NF</a>
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="/">Sekolah Tinggi Teknologi Terpadu Nurul Fikri</a>.</strong> All rights
    reserved.
</footer>