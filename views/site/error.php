<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<section class="content">

    <div class="error-page container">
        <h2 class="headline text-info" style="margin: 5% auto;"><i class="fas fa-warning text-yellow"></i></h2>

        <div class="error-content">
            <?php if ($message !== "Your request is not supported by our team."):?>
                <h3><?= $name ?></h3>
            <?php else: ?>
                <h3>Not Supported (#909)</h3>
            <?php endif ?>

            <h5 class="text-danger">
                <strong>
                    <?= nl2br(Html::encode($message)) ?>
                </strong>
            </h5>

            <p>
                The above error occurred while the Web server was processing your request.
                Please contact us if you think this is a server error. Thank you. you may <a href='<?= Yii::$app->homeUrl ?>'>return to dashboard</a>
            </p>
        </div>
    </div>

</section>
