<?php

namespace app\traits;

trait TimeTrait 
{
    public function setTime($time) 
    {
        return date('U', $time);
    }

    public function getTime($time) 
    {
        return date('D d M, H:i', $time);
    }

    public function getDate($date) 
    {
        return date('l, d M Y', $date);
    }

    public function setDate($date) 
    {
        return date('Y-M-d', $date);
    }
}