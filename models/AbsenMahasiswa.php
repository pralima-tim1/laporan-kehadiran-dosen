<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dynamic_absen_mahasiswa".
 *
 * @property int $id
 * @property string $mhs_id
 * @property int $kelas_id
 * @property int $status_id
 * @property int $absen_dosen_id
 *
 * @property AbsenDosen $absenDosen
 * @property Kelas $kelas
 * @property Status $status
 * @property Mahasiswa $mhs
 */
class AbsenMahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dynamic_absen_mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'mhs_id', 'kelas_id', 'status_id', 'absen_dosen_id'], 'required'],
            [['id', 'kelas_id', 'status_id', 'absen_dosen_id'], 'integer'],
            [['mhs_id'], 'string', 'max' => 255],
            [['absen_dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => AbsenDosen::className(), 'targetAttribute' => ['absen_dosen_id' => 'id']],
            [['kelas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['kelas_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['mhs_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['mhs_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mhs_id' => 'Mhs ID',
            'kelas_id' => 'Kelas ID',
            'status_id' => 'Status ID',
            'absen_dosen_id' => 'Absen Dosen ID',
        ];
    }

    /**
     * Gets query for [[AbsenDosen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAbsenDosen()
    {
        return $this->hasOne(AbsenDosen::className(), ['id' => 'absen_dosen_id']);
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id' => 'kelas_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * Gets query for [[Mhs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMhs()
    {
        return $this->hasOne(Mahasiswa::className(), ['id' => 'mhs_id']);
    }
}
