<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dynamic_absen_dosen".
 *
 * @property int $id ID
 * @property string|null $dosen_id Dosen
 * @property int|null $kelas_id Kelas
 * @property string|null $occurred_at Tanggal
 * @property string|null $hour_start Jam Mulai
 * @property string|null $hour_end Jam Selesai
 * @property string|null $theory Materi
 * @property int|null $conference_for Tatap Muka ke
 *
 * @property Kelas $kelas
 * @property Dosen $dosen
 * @property AbsenMahasiswa[] $AbsenMahasiswas
 */
class AbsenDosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dynamic_absen_dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'kelas_id', 'conference_for'], 'integer'],
            [['occurred_at', 'hour_start', 'hour_end'], 'safe'],
            [['theory'], 'string'],
            [['dosen_id'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['kelas_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['kelas_id' => 'id']],
            [['dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['dosen_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dosen_id' => 'Dosen',
            'kelas_id' => 'Kelas',
            'occurred_at' => 'Tanggal',
            'hour_start' => 'Jam Mulai',
            'hour_end' => 'Jam Selesai',
            'theory' => 'Materi',
            'conference_for' => 'Tatap Muka ke',
        ];
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id' => 'kelas_id']);
    }

    /**
     * Gets query for [[Dosen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id' => 'dosen_id']);
    }

    /**
     * Gets query for [[AbsenMahasiswas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAbsenMahasiswas()
    {
        return $this->hasMany(AbsenMahasiswa::className(), ['absen_dosen_id' => 'id']);
    }
}
