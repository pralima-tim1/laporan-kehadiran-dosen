<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "static_matakuliah".
 *
 * @property int $id ID
 * @property string|null $code Kode
 * @property string|null $name Nama
 * @property int|null $sks SKS
 * @property string|null $abbr Singkatan
 * @property int|null $total_student Total Mahasiswa
 *
 * @property Kelas[] $Kelas
 */
class Matakuliah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'static_matakuliah';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sks', 'total_student'], 'integer'],
            [['code'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 255],
            [['abbr'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Kode',
            'name' => 'Nama',
            'sks' => 'SKS',
            'abbr' => 'Singkatan',
            'total_student' => 'Total Mahasiswa',
        ];
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['mk_id' => 'id']);
    }
}
