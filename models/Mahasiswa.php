<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "static_mahasiswa".
 *
 * @property string $id NIM
 * @property string $name Nama
 * @property int $prodi_id Prodi
 * @property string|null $rombel Rombongan Belajar
 *
 * @property AbsenMahasiswa[] $AbsenMahasiswas
 * @property Prodi $prodi
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'static_mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'prodi_id'], 'required'],
            [['prodi_id'], 'integer'],
            [['id', 'name', 'rombel'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['prodi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prodi::className(), 'targetAttribute' => ['prodi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'NIM',
            'name' => 'Nama',
            'prodi_id' => 'Prodi',
            'rombel' => 'Rombongan Belajar',
        ];
    }

    /**
     * Gets query for [[AbsenMahasiswas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAbsenMahasiswas()
    {
        return $this->hasMany(AbsenMahasiswa::className(), ['mhs_id' => 'id']);
    }

    /**
     * Gets query for [[Prodi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdi()
    {
        return $this->hasOne(Prodi::className(), ['id' => 'prodi_id']);
    }
}
