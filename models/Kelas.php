<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "static_kelas".
 *
 * @property int $id id
 * @property int|null $mk_id Mata Kuliah
 * @property string|null $dosen_id Dosen
 * @property int|null $prodi_id Prodi
 * @property int|null $ka_id Kalender Akademik
 * @property int|null $start_time Jam ke-
 * @property int|null $end_time Sampai ke-
 * @property int|null $status Status
 * @property string|null $day Hari
 * @property string|null $semester Semester
 * @property int|null $total_student Total Mahasiswa
 * @property string|null $date_start Tanggal Mulai
 * @property string|null $date_end Tanggal Selesai
 * @property string|null $start_at Jam Mulai
 * @property string|null $end_at Jam Selesai
 *
 * @property AbsenDosen[] $AbsenDosens
 * @property AbsenMahasiswa[] $AbsenMahasiswas
 * @property Matakuliah $mk
 * @property Prodi $prodi
 * @property Dosen $dosen
 * @property KalenderAkademik $ka
 */
class Kelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'static_kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_start', 'date_end', 'start_at', 'end_at'], 'safe'],
            [['dosen_id'], 'string', 'max' => 255],
            [['day'], 'string', 'max' => 10],
            [['semester'], 'string', 'max' => 5],
            [['prodi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prodi::className(), 'targetAttribute' => ['prodi_id' => 'id']],
            [['dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['dosen_id' => 'id']],
            [['ka_id'], 'exist', 'skipOnError' => true, 'targetClass' => KalenderAkademik::className(), 'targetAttribute' => ['ka_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'mk_id' => 'Mata Kuliah',
            'dosen_id' => 'Dosen',
            'prodi_id' => 'Prodi',
            'ka_id' => 'Kalender Akademik',
            'start_time' => 'Jam ke-',
            'end_time' => 'Sampai ke-',
            'status' => 'Status',
            'day' => 'Hari',
            'semester' => 'Semester',
            'total_student' => 'Total Mahasiswa',
            'date_start' => 'Tanggal Mulai',
            'date_end' => 'Tanggal Selesai',
            'start_at' => 'Jam Mulai',
            'end_at' => 'Jam Selesai',
        ];
    }

    /**
     * Gets query for [[AbsenDosens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAbsenDosens()
    {
        return $this->hasMany(AbsenDosen::className(), ['kelas_id' => 'id']);
    }

    /**
     * Gets query for [[AbsenMahasiswas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAbsenMahasiswas()
    {
        return $this->hasMany(AbsenMahasiswa::className(), ['kelas_id' => 'id']);
    }

    /**
     * Gets query for [[Mk]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMk()
    {
        return $this->hasOne(Matakuliah::className(), ['id' => 'mk_id']);
    }

    /**
     * Gets query for [[Prodi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdi()
    {
        return $this->hasOne(Prodi::className(), ['id' => 'prodi_id']);
    }

    /**
     * Gets query for [[Dosen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id' => 'dosen_id']);
    }

    /**
     * Gets query for [[Ka]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKa()
    {
        return $this->hasOne(KalenderAkademik::className(), ['id' => 'ka_id']);
    }
}
