<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "static_dosen".
 *
 * @property string $id
 * @property string $name
 *
 * @property AbsenDosen[] $AbsenDosens
 * @property Kelas[] $Kelas
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'static_dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[AbsenDosens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAbsenDosens()
    {
        return $this->hasMany(AbsenDosen::className(), ['dosen_id' => 'id']);
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasMany(Kelas::className(), ['dosen_id' => 'id']);
    }
}
